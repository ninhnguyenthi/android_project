package com.demo.project;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import com.demo.project.model.Group;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {
    private static final String TAG = "HomeFragment";
    ImageView imgFilter;
    private final List<Group> listGroup = new ArrayList<>();

    public HomeFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initData();
        HomeAdapter adapter = new HomeAdapter(getActivity(), listGroup);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        RecyclerView rvPackages = view.findViewById(R.id.rv_packages);
        rvPackages.setLayoutManager(layoutManager);
        rvPackages.setAdapter(adapter);
        imgFilter = view.findViewById(R.id.img_filter);
        imgFilter.setOnClickListener(v -> {
            Log.d(TAG,"onClick: opening dialog");
            openDialogFragment();
        });

        return view;
    }

    private void initData() {
        listGroup.add(new Group("000001497", "Ví ngân lượng", "12 tháng", "12.000.000đ", "1.000.000đ"));
        listGroup.add(new Group("000001497", "Ví ngân lượng", "12 tháng", "12.000.000đ", "1.000.000đ"));
        listGroup.add(new Group("000001497", "Ví ngân lượng", "12 tháng", "12.000.000đ", "1.000.000đ"));

    }

    private void openDialogFragment(){
        final Dialog dialog = new Dialog(this.getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.fragment_home_filter);
        Window window = dialog.getWindow();
        if (window == null){
            return;
        }
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        WindowManager.LayoutParams windowAttributes = window.getAttributes();
        windowAttributes.gravity = Gravity.CENTER;
        window.setAttributes(windowAttributes);
        dialog.show();

    }

}