package com.demo.project.model;

public class Group {
    private String code;
    private String monthly_interest;
    private String time;
    private String total_profit;
    private String form;

    public Group(String code, String form, String time, String total_profit, String monthly_interest) {
        this.code = code;
        this.form = form;
        this.time = time;
        this.total_profit = total_profit;
        this.monthly_interest = monthly_interest;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTotal_profit() {
        return total_profit;
    }

    public void setTotal_profit(String total_profit) {
        this.total_profit = total_profit;
    }

    public String getMonthly_interest() {
        return monthly_interest;
    }

    public void setMonthly_interest(String monthly_interest) {
        this.monthly_interest = monthly_interest;
    }
}
