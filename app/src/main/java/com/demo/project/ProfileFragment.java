package com.demo.project;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class ProfileFragment extends Fragment {
    public static final String SHARE_PREF_NAME = "useFile";
    public static final String KEY_ACTIVE = "active";
    public static final int REQUEST_CODE_CAMERA = 22;
    public static final int REQUEST_CODE_GALLERY = 11;
    private static final int RESULT_OK = 0 ;
    private Button btLogout;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ImageView imgProfile, imgEditPhoto;
    private ConstraintLayout ctlPersonalInfo;
    File photoFile = null ;
    String mCurrentPhotoPath;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ctlPersonalInfo = view.findViewById(R.id.ctl_personal_info);
        ctlPersonalInfo.setOnClickListener(v ->
                goToDetailFragment()
        );
        btLogout = view.findViewById(R.id.btn_logout);
        btLogout.setOnClickListener(v -> {
            Toast.makeText(getActivity(), "Logout success !", Toast.LENGTH_SHORT).show();
            logout();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        });
        imgProfile = view.findViewById(R.id.img_profile);
        imgEditPhoto = view.findViewById(R.id.img_edit_photo);
        imgEditPhoto.setOnClickListener(v ->
                showPictureDialog()
        );
        requestPermission();
        return view;
    }

    public void goToDetailFragment() {
        if (getActivity() != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            PersonalInformationFragment personalInformationFragment = new PersonalInformationFragment();
            fragmentTransaction.replace(R.id.frame_container, personalInformationFragment);
            fragmentTransaction.addToBackStack(PersonalInformationFragment.TAG);
            fragmentTransaction.commit();
        }
    }

    public void logout() {
        if (getActivity() != null) {
            sharedPreferences = getActivity().getSharedPreferences(SHARE_PREF_NAME, Context.MODE_PRIVATE);
        }
        editor = sharedPreferences.edit();
        editor.putBoolean(KEY_ACTIVE, false);
        editor.commit();
    }

    private void showPictureDialog() {
        android.app.AlertDialog.Builder pictureDialog = new android.app.AlertDialog.Builder(getActivity());
        pictureDialog.setTitle(R.string.select_action);
        String[] pictureDialogItems = {
                getString(R.string.take_photo),
                getString(R.string.choose_from_gallery)};
        pictureDialog.setItems(pictureDialogItems,
                (dialog, which) -> {
                    switch (which) {
                        case 0:
                            takePhotoFromCamera();
                            break;
                        case 1:
                            choosePhotoFromGallery();
                    }
                });
        pictureDialog.show();
    }


    @SuppressLint("QueryPermissionsNeeded")
    public void takePhotoFromCamera() {
        if (getActivity() != null) {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
            }
        }
    }

    public void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, REQUEST_CODE_GALLERY);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bitmap bitmap;
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == REQUEST_CODE_GALLERY) {
                if (data != null) {
                    Uri contentURI = data.getData();
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(requireActivity().getContentResolver(), contentURI);
                        imgProfile.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(getActivity(), "Failed!", Toast.LENGTH_SHORT).show();
                    }
                }
                {
                    Toast.makeText(getActivity(), "Data not found", Toast.LENGTH_SHORT).show();
                }
            } else if (requestCode == REQUEST_CODE_CAMERA) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap  = (Bitmap) extras.get("data");
                imgProfile.setImageBitmap(imageBitmap);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.FROYO)
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void requestPermission() {
        PermissionListener permissionListener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {

            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                Toast.makeText(getContext(), "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
        if (getContext() != null) {
            TedPermission.with(getContext())
                    .setPermissionListener(permissionListener)
                    .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                    .check();
        }
    }

}
