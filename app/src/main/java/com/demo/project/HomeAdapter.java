package com.demo.project;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demo.project.model.Group;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter {
    Activity activity;
    List<Group> listGroup;

    public HomeAdapter(Activity activity, List<Group> listGroup) {
        this.activity = activity;
        this.listGroup = listGroup;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = activity.getLayoutInflater().inflate(R.layout.item_investment_package, parent, false);
        GroupViewHolder holder;
        holder = new GroupViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        GroupViewHolder vh = (GroupViewHolder) holder;
        Group model = listGroup.get(position);
        vh.tvInvestedCode.setText(model.getCode());
        vh.tvMonthlyInterest.setText(model.getMonthly_interest());
        vh.tvInvestmentTime.setText(model.getTime());
        vh.tvTotalProfit.setText(model.getTotal_profit());
    }

    @Override
    public int getItemCount() {
        return listGroup.size();
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder {
        TextView tvInvestedCode;
        TextView tvMonthlyInterest;
        TextView tvInvestmentTime;
        TextView tvTotalProfit;
        TextView tvForm;

        public GroupViewHolder(@NonNull View itemView) {
            super(itemView);

            tvInvestedCode = itemView.findViewById(R.id.tv_invested_code);
            tvMonthlyInterest = itemView.findViewById(R.id.tv_monthly_interest);
            tvInvestmentTime = itemView.findViewById(R.id.tv_investment_time);
            tvTotalProfit = itemView.findViewById(R.id.tv_total_profit);
            tvForm = itemView.findViewById(R.id.tv_form);


        }
    }
}
