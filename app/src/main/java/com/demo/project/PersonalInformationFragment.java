package com.demo.project;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class PersonalInformationFragment extends Fragment {

    public static final String TAG = PersonalInformationFragment.class.getName();
    ImageView imgPersonalArrow;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal_infomation, container, false);
        imgPersonalArrow = view.findViewById(R.id.img_icon_arrow_personal);
        imgPersonalArrow.setOnClickListener(v -> requireActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE));
        return view;
    }
}
